﻿using Microsoft.CodeAnalysis.CSharp.Scripting;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoslynCalc {
    public partial class FormMain : Form {
        public FormMain() {
            InitializeComponent();
        }

        private async void tbxExpression_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Enter) {
                try {
                    string exp = this.tbxExpression.Text;
                    object res = await CSharpScript.EvaluateAsync(exp);
                    tbxEval.Text = res.ToString();
                } catch (Exception ex) {
                    tbxEval.Text = ex.Message;
                }
            } else {
                string exp = this.tbxExpression.Text;
                object res = this.tbxEval.Text;
                lbxResult.Items.Insert(0, $"{exp}={res}");
                tbxExpression.Clear();
            }
        }

        private void lbxResult_SelectedIndexChanged(object sender, EventArgs e) {
            string text = this.lbxResult.SelectedItem.ToString();
            string exp = text.Split(new string[] { " = " }, StringSplitOptions.None)[0];
            this.tbxExpression.Text = exp;
        }
    }
}
